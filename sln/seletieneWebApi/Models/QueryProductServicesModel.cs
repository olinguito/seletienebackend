﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace seletieneWebApi.Models
{
    public class QueryProductServicesModel
    {
        public enum Order { none,stars,date,lexic,provider}
        public string q { get; set; }
        public int minStars { get; set; }
        public string city { get; set; }
        public int cityId { get; set; }
        public string  department { get; set; }
        public int departmentId { get; set; }
        public Order order { get; set; }
        public int results { get; set; }
        public int page { get; set; }
        public ProductService.TypeOffering type { get; set; }
    }
}
