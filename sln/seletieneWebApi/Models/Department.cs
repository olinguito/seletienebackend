﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace seletieneWebApi.Models
{
    public class Department
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public virtual ICollection<City> Cities { get; set; }
        public string PostalCodeGeo { get; set; }

        public DepartmentSimplified GetSimplified()
        {
            return new DepartmentSimplified
            {
                ID = this.ID,
                Name = this.Name,
                PostalCode=this.PostalCodeGeo
            };
        }
        public Object GetParseable()
        {
            return new
            {
                ID=this.ID,
                Name=this.Name,
                PostalCodeGeo=this.PostalCodeGeo,
                Cities=this.Cities.Select(cit=>new {cit.ID,cit.Name,cit.PostalCodeGeo})
            };
        }

    }
    public class DepartmentSimplified
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string PostalCode { get; set; }
    }
}