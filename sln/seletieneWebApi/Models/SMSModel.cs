﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace seletieneWebApi.Models
{
    public class SMSModel
    {
        public string MENSAJE { get; set; }
        public string NUMERO_CELULAR { get; set; }
        public string OPERADOR { get; set; }
    }
}