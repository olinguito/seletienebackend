﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace seletieneWebApi.Models
{
    public class RecoverPasswordModel
    {
        public string NewPassword { get; set; }
        public string Token { get; set; }
    }
}