﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace seletieneWebApi.Models
{
    public class ProductServiceSimplified
    {
        public ProductService.TypeOffering Type
        {
            get;
            set;
        }
        public String Title { get; set; }
        public String Description { get; set; }
        public decimal Rating { get; set; }
        public int numberRatings { get; set; }
        public string ImageFile { get; set; }
        public DateTime? RegistrationTime { get; set; }
        public bool DPSValidated { get; set; }
        public virtual City City { get; set; }
        public virtual DepartmentSimplified Department { get; set; }
        public Category Category { get; set; }

        public UserUltraSimplified Owner { get; set; }
    }
    public class ProductService
    {
        public enum TypeOffering { PRODUCT, SERVICE };

        public int Id { get; set; }
        [Required]
        public TypeOffering Type
        {
            get;
            set;
        }
        [Required]
        public String Title { get; set; }
        public String Description { get; set; }
        public decimal Rating { get; set; }
        public int numberRatings { get; set; }
        public string OwnerId { get; set; }
        public string OwnerName { get; set; }
        public string ImageFile { get; set; }
        public DateTime? RegistrationTime { get; set; }
        public bool DPSValidated { get; set; }


        public int? CityId { get; set; }
        public virtual City City { get; set; }

        public int? DepartmentId { get; set; }
        public virtual Department Department { get; set; }

        public int? CategoryId { get; set; }
        public virtual Category Category { get; set; }


        public UserUltraSimplified GetOwner() { 
            return new SeletieneWebApiContext().Users.Find(OwnerId).GetUltraSimplified();
        }
        public ProductServiceSimplified GetDetail()
        {
            return new ProductServiceSimplified
            {
                Type = this.Type,
                Category = this.Category,
                City = this.City,
                Department = this.Department.GetSimplified(),
                Description = this.Description,
                DPSValidated = this.DPSValidated,
                ImageFile=this.ImageFile,
                numberRatings=this.numberRatings,
                Rating=this.Rating,
                Owner=this.GetOwner(),
                RegistrationTime=this.RegistrationTime,
                Title=this.Title
            };
        }
    }
}