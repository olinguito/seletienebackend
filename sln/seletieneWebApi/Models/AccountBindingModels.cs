﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace seletieneWebApi.Models
{
    // Models used as parameters to AccountController actions.

    public class AddExternalLoginBindingModel
    {
        [Required]
        [Display(Name = "External access token")]
        public string ExternalAccessToken { get; set; }
    }

    public class ChangePasswordBindingModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class RegisterBindingModel
    {
        public bool isSMSuser { get; set; }
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }


        /// <summary>
        /// Cédula de ciudadanía
        /// </summary>
        public string UserId { get; set; }

        [Required]
        public string Name { get; set; }
        //Email already included in IdentityUser

        /// <summary>
        /// celular
        /// </summary>
        public string MobileNumber { get; set; }
        public string PhoneNumber { get; set; }
        public string City { get; set; }
        public string Department { get; set; }
        public int CityID { get; set; }
        public int DepartmentID { get; set; }
    }

    public class RegisterExternalBindingModel
    {

        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        public string token { get; set; }


        /// <summary>
        /// Cédula de ciudadanía
        /// </summary>
        public string UserId { get; set; }
        public string Name { get; set; }
        
        /// <summary>
        /// celular
        /// </summary>
        public string MobileNumber { get; set; }
        public string PhoneNumber { get; set; }
        public int cityId { get; set; }
        public int departmentId { get; set; }
    }

    public class RemoveLoginBindingModel
    {
        [Required]
        [Display(Name = "Login provider")]
        public string LoginProvider { get; set; }

        [Required]
        [Display(Name = "Provider key")]
        public string ProviderKey { get; set; }
    }

    public class SetPasswordBindingModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}
