﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;

namespace seletieneWebApi.Models
{
    public class SeletieneWebApiContext : IdentityDbContext<User>
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public SeletieneWebApiContext() //: base("name=seletieneWebApiContext")
                                     : base("seletieneWebApiContext", throwIfV1Schema: false)
        {
        }

        public System.Data.Entity.DbSet<seletieneWebApi.Models.ProductService> ProductServices { get; set; }
        public System.Data.Entity.DbSet<seletieneWebApi.Models.Department> Departments { get; set; }
        public System.Data.Entity.DbSet<seletieneWebApi.Models.City> Cities { get; set; }
        public System.Data.Entity.DbSet<seletieneWebApi.Models.Category> Categories { get; set; }

        
        public static SeletieneWebApiContext Create()
        {            
            
            var migratorConfig = new Migrations.Configuration();
            var dbMigrator = new DbMigrator(migratorConfig);
            try
            {
                dbMigrator.Update();
            }
            catch(Exception e)
            {
                throw e;
            }
            return new SeletieneWebApiContext();
        }
    
    }
}
