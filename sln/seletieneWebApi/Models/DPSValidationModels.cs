﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace seletieneWebApi.Models
{
    public class ValidateProviderBindingModel
    {
        [Required]
        public string UserDBId { get; set; }
        public Boolean UserValidated { get; set; }
        public Boolean PhoneNumberConfirmed { get; set; }
        public Boolean EmailConfirmed { get; set; }
    }
    public class ValidateProductServiceBindingModel
    {
        public int ProductServiceId { get; set; }
        public bool validated { get; set; }
    }
}