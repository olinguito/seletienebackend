﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace seletieneWebApi.Models
{
    public class UserSimplified
    {
        public string ID { get; set; }
        public string UserId { get; set; }
        public string FacebookUserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public DateTime? RegistrationTime { get; set; }
        public virtual City City { get; set; }
        public virtual DepartmentSimplified Department { get; set; }
        public bool DPSValidated;
        public bool DPSCallcenterValidated;
    }

    public class UserUltraSimplified
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public bool DPSValidated;
        public bool DPSCallcenterValidated;
    }
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class User : IdentityUser
    {
        public UserSimplified GetSimplified()
        {
            return new UserSimplified()
            {
                City = this.City,
                Department = this.Department==null?null:this.Department.GetSimplified(),
                DPSValidated = this.DPSValidated,
                Email = this.Email,
                FacebookUserId = this.FacebookUserId,
                MobileNumber = this.MobileNumber,
                PhoneNumber = this.PhoneNumber,
                RegistrationTime = this.RegistrationTime,
                UserId = this.UserId,
                Name = this.Name,
                ID=this.Id
            };
        }
        public UserUltraSimplified GetUltraSimplified()
        {
            return new UserUltraSimplified()
            {
                DPSValidated = this.DPSValidated,
                Email = this.Email,
                MobileNumber = this.MobileNumber,
                PhoneNumber = this.PhoneNumber,
                Name = this.Name,
                ID = this.Id
            };
        }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
        
        /// <summary>
        /// Cédula de ciudadanía
        /// </summary>
        public string UserId { get; set; }
        public string FacebookUserId { get; set; }
        private string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name=value;
                if (ProductsAndServices != null)
                {
                    foreach (var item in ProductsAndServices)
                    {
                        item.OwnerName = name;
                    }
                }   
        } }
        //Email already included in IdentityUser
        
        /// <summary>
        /// celular
        /// </summary>
        public string MobileNumber { get; set; }
        public DateTime? RegistrationTime { get; set; }

        public int? CityId { get; set; }
        public int? DepartmentId { get; set; }
        public virtual City City { get; set; }
        public virtual Department Department { get; set; }

        /// <summary>
        /// Validación de usuario por parte del DPS. por defecto es false;
        /// </summary>
        public bool DPSValidated;
        public bool DPSCallcenterValidated;

        public virtual ICollection<ProductService> ProductsAndServices { get; set; }

        public virtual ICollection<ProductService> Favorites { get; set; }


        internal bool satisfyDataForProvider()
        {//TODO Verify conditions here
            if (String.IsNullOrWhiteSpace(Name) ||
                String.IsNullOrWhiteSpace(MobileNumber) && String.IsNullOrWhiteSpace(PhoneNumber)
                ||String.IsNullOrWhiteSpace(UserId))
                return false;
            else return true;
        }
    }

    public class FacebookLoginModel
    {
        public string token { get; set; }
        public string username { get; set; }
        public string userid { get; set; }
    }

    public class FacebookUserViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public string Username { get; set; }
        public string Email { get;set; }
        public string Gender { get; set; }
    }

}