﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using seletieneWebApi.Models;

using Microsoft.AspNet.Identity;
using System.Web;
using System.IO;
using System.Threading.Tasks;
using System.Web.UI;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;

namespace seletieneWebApi.Controllers
{
    [Authorize]
    public class ProductServicesController : ApiController
    {
        private SeletieneWebApiContext db = new SeletieneWebApiContext();

        /// <summary>
        /// Consulta los proudctos y servicios disponibles
        /// </summary>
        /// <param name="results">{# resultados}</param>
        /// <param name="page">{# página}</param>
        /// <param name="q">{texto a encontrar}</param>
        /// <param name="minStars">{mínimo de estrellas}</param>
        /// <param name="cityId">Tiene prioridad sobre "city"</param>
        /// <param name="departmentId">Tiene prioridad sobre "department"</param>
        /// <param name="city">Nombre de ciudad</param>
        /// <param name="department">Nombre de departamento</param>
        /// <param name="categoryId">{category id}</param>
        /// <param name="category">{category name}</param>
        /// <param name="order">"stars"|"date"|"lexic"|"provider"</param>
        /// <param name="type">"product"|"service"</param>
        /// <param name="ignoreDpsValidation">Por defecto es "false"</param>
        /// <returns>Listado de productos y servicios según los filtros</returns>
        public IQueryable<ProductServiceSimplified> GetProductServices(int? results = null, int? page = null, string q = null, int? minStars = null, 
            int? cityId = null, int? departmentId = null,string city=null,string department=null,
            int? categoryId=null,string category=null,
            QueryProductServicesModel.Order? order = null, ProductService.TypeOffering? type = null,
            bool? ignoreDpsValidation=false)
        {
            //return db.ProductServices;
            //GET /api/ProductServices?q=TEXTO_PARA_BUSCAR&minStars=3&city=bogota&order=stars&results=10&page=2
            QueryProductServicesModel qs = new QueryProductServicesModel();
            var parametros = Request.RequestUri.ParseQueryString();

            if (minStars != null) qs.minStars = minStars.Value;
            if (cityId != null) qs.cityId = cityId.Value;
            if (departmentId != null) qs.departmentId = departmentId.Value;
            if (results != null) qs.results = results.Value;
            if (page != null) qs.page = page.Value;

            qs.q = q;
            //qs.q = q??q.ToString();
            qs.city = city;
            qs.department = department;
            if (order != null)
            {
                qs.order = order.Value;
            }
            if (type != null)
            {
                qs.type = type.Value;
            }
            
            
            var resp = from p in db.ProductServices where (ignoreDpsValidation??false||p.DPSValidated)&&p.Rating >= qs.minStars select p;
            if (categoryId != null)
            {
                resp = from p in resp where p.CategoryId == categoryId select p;
            }
            else if (category != null)
            {
                resp = from p in resp where p.Category.Name.Contains(category) select p;
            }
            if (!String.IsNullOrWhiteSpace(qs.q))
            {
                resp = from p in resp where p.Title.Contains(qs.q) || p.Description.Contains(qs.q) select p;
            }
            if (qs.cityId > 0)
            {
                resp = from p in resp where p.CityId == qs.cityId select p;
            }
            else if (!String.IsNullOrWhiteSpace(qs.city))
            {
                resp = from p in resp where p.City.Name.Contains(qs.city) select p;
            }
            else if (qs.departmentId > 0)
            {
                resp = from p in resp where p.DepartmentId == qs.departmentId select p;
            }
            else if (!String.IsNullOrWhiteSpace(qs.department))
            {
                resp = from p in resp where p.Department.Name.Contains(qs.department) select p;
            }

            if (type != null)
            {
                resp = resp.Where(p => p.Type == qs.type);
            }
            switch (qs.order)
            {
                case QueryProductServicesModel.Order.date:
                    resp = from p in resp orderby p.RegistrationTime descending select p;
                    break;
                case QueryProductServicesModel.Order.lexic:
                    resp = from p in resp orderby p.Title select p;
                    break;
                case QueryProductServicesModel.Order.provider:
                    resp = from p in resp orderby p.OwnerName select p;
                    break;
                case QueryProductServicesModel.Order.stars:
                    resp = from p in resp orderby p.Rating descending select p;
                    break;
                default:
                    resp = from p in resp orderby p.Title select p;
                    break;
            }
            if (qs.results > 0)
            {
                resp = resp.Skip(qs.results * qs.page).Take(qs.results);
            }
            List<ProductService> lista = resp.ToList();
            List<ProductServiceSimplified> listaResp = new List<ProductServiceSimplified>();
            foreach(ProductService ps in lista)
            {
                listaResp.Add(ps.GetDetail());
            }

            //return resp;
            return listaResp.AsQueryable();
        }

        //PUT: api/favorite
        [Route("api/ProductServices/Categories")]
        [HttpGet]
        public IQueryable<Category> Categories()
        {
            return db.Categories;
        }

        //PUT: api/favorite
        [Route("api/ProductServices/Rate")]
        [HttpPut]
        [Authorize]
        public IHttpActionResult Rate(int productServiceId, decimal newRating)
        {
            try
            {
                var prod = db.ProductServices.Find(productServiceId);
                if (prod != null)
                {   
                    prod.Rating = prod.Rating * (prod.numberRatings) + newRating;
                    prod.numberRatings++;
                    prod.Rating /= prod.numberRatings;
                    db.SaveChanges();
                    return Ok();
                }
                return NotFound();// ("Product or service not found");
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        // GET: api/ProductServices/5
        //[ResponseType(typeof(ProductService))]
        public IHttpActionResult GetProductService(int id)
        {
            ProductService productService = db.ProductServices.Find(id);
            if (productService == null)
            {
                return NotFound();
            }

            //ProductServiceDetailed det = new ProductServiceDetailed { ProductService = productService, 
            //productService.Owner = productService.GetOwner();
            return Ok(productService.GetDetail());
        }

        

        // PUT: api/ProductServices/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutProductService(int id, ProductService productService)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != productService.Id)
            {
                return BadRequest();
            }
            

            db.Entry(productService).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductServiceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Agrega el producto a mis favoritos
        /// </summary>
        /// <param name="productServiceId"></param>
        /// <returns></returns>
        [Authorize]
        [Route("api/ProductServices/Favorite")]
        [HttpPut]
        public async Task<IHttpActionResult> AddFavorite(int productServiceId)
        {
            var prod=await db.ProductServices.FindAsync(productServiceId);
            if(prod!=null)
            {
                db.Users.Find(User.Identity.GetUserId()).Favorites.Add(prod);
                db.SaveChanges();
                return Ok();
            }
            return NotFound();
        }

        
        ///<summary>
        ///Elimina el producto de mis favoritos
        ///</summary>
        [Authorize]
        [Route("api/ProductServices/Favorite")]
        [HttpDelete]
        public async Task<IHttpActionResult> RemoveFavorite(int productServiceId)
        {
            var prod = await db.ProductServices.FindAsync(productServiceId);
            if (prod != null)
            {
                db.Users.Find(User.Identity.GetUserId()).Favorites.Remove(prod);
                db.SaveChanges();
                return Ok();
            }
            return NotFound();
        }


        // POST: api/ProductServices
        /// <summary>
        /// Crea un nuevo producto o servicio.
        /// Type and title required: 0 for Product, 1 for Service
        /// </summary>
        /// <param name="productService"></param>
        /// <returns></returns>
        [ResponseType(typeof(ProductService))]
        [Authorize]//TODO (Roles="Provider")
        public IHttpActionResult PostProductService(ProductService productService)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var ownerid = User.Identity.GetUserId();

            using(var ctx=new SeletieneWebApiContext())
            {
                productService.RegistrationTime = DateTime.Now;
                User owner = ctx.Users.Find(ownerid);
                productService.OwnerId = ownerid;
                productService.OwnerName = owner.Name;
                if(productService.City==null && productService.Department==null)
                {
                    if (owner.CityId > 0 && owner.DepartmentId > 0)
                    {
                        productService.CityId = owner.CityId;
                        productService.DepartmentId = owner.DepartmentId;
                    }
                    if (owner.ProductsAndServices == null)
                    {
                        owner.ProductsAndServices = new HashSet<ProductService>();
                    }
                }
                
                owner.ProductsAndServices.Add(productService);
                
                ctx.ProductServices.Add(productService);
                ctx.SaveChanges();
            }

            return CreatedAtRoute("DefaultApi", new { id = productService.Id }, productService);
        }

        /// <summary>
        /// Agrega una imagen a un producto o servicio existente, actualiza el producto o servicio con la url de la imagen cargada
        /// </summary>
        /// <param name="ProductoServicioId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ProductServices/Image")]
        [Authorize]
        public async Task<IHttpActionResult> UploadImage(int ProductoServicioId)
        {
            if(!Request.Content.IsMimeMultipartContent())
            {
                return BadRequest("Expecting MIME multipart content");
            }
            string fileSaveLocation = HttpContext.Current.Server.MapPath("~/UploadedImages");
            
            MultipartFormDataStreamProvider provider = new MultipartFormDataStreamProvider(fileSaveLocation);
            
            try {
                await Request.Content.ReadAsMultipartAsync(provider);
                foreach (MultipartFileData file in provider.FileData) {
                    using (var ctx = new SeletieneWebApiContext())
                    {
                        var prod = ctx.ProductServices.Find(ProductoServicioId);
                        if (prod != null)
                        {
                            string extension = "";
                            foreach (var he in file.Headers)
                            {
                                if (he.Key.Equals("Content-Type"))
                                    extension = "."+he.Value.First().Split('/')[1];
                            }
                            if(!extension.Equals(""))
                            {
                                File.Copy(file.LocalFileName, file.LocalFileName + extension);
                                File.Delete(file.LocalFileName);
                            }                            
                            prod.ImageFile = file.LocalFileName+extension;
                            string[] partes = prod.ImageFile.Split('\\');
                            prod.ImageFile = "/UploadedImages/" + partes[partes.Length - 1];
                        }
                        ctx.SaveChanges();
                        db.SaveChanges();
                    }
                } 
                return Ok();
            }
            catch (System.Exception e) {
                return InternalServerError(e);
            }

        }

        
        // DELETE: api/ProductServices/5
        /// <summary>
        /// Requiere autenticación de un validador de DPS
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ResponseType(typeof(ProductService))]
        [Authorize(Roles="DPSValidator")]
        public IHttpActionResult DeleteProductService(int id)
        {
            ProductService productService = db.ProductServices.Find(id);
            if (productService == null)
            {
                return NotFound();
            }

            db.ProductServices.Remove(productService);
            db.SaveChanges();

            return Ok(productService);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProductServiceExists(int id)
        {
            return db.ProductServices.Count(e => e.Id == id) > 0;
        }

        
    }
}