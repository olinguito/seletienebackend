﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using seletieneWebApi.Models;

namespace seletieneWebApi.Controllers
{
    public class DepartmentsController : ApiController
    {
        private SeletieneWebApiContext db = new SeletieneWebApiContext();

        // GET: api/Departments
        [Route("api/Departments")]
        public IEnumerable<DepartmentSimplified> GetDepartments()
        {
            List<DepartmentSimplified> resp = new List<DepartmentSimplified>();
            var res = db.Departments.AsEnumerable();
            foreach (var item in res)
            {
                resp.Add(item.GetSimplified());
            }
            return resp;
        }

        // GET: api/Departments/5
        /// <summary>
        /// Return detail of department with all cities
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ResponseType(typeof(Department))]
        public async Task<IHttpActionResult> GetDepartment(int id)
        {
            Department department = await db.Departments.FindAsync(id);
            if (department == null)
            {
                return NotFound();
            }
            
            return Ok(department);
        }


        
        // PUT: api/Departments/5
        [ResponseType(typeof(void))]
        [Route("api/Departments")]
        [Authorize(Roles = "DPSValidator")]
        public async Task<IHttpActionResult> PutDepartment(int id, Department department)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != department.ID)
            {
                return BadRequest();
            }

            db.Entry(department).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DepartmentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Departments
        [ResponseType(typeof(Department))]
        [Route("api/Departments")]
        public async Task<IHttpActionResult> PostDepartment(Department department)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Departments.Add(department);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = department.ID }, department);
        }


        // GET: api/Departments
        [Route("api/Cities")]
        [HttpGet]
        public IQueryable<City> GetCities()
        {
            return db.Cities;
        }

        // GET: api/Departments/5
        [ResponseType(typeof(City))]
        [Route("api/Cities")]
        [HttpGet]
        public async Task<IHttpActionResult> GetCity(int id)
        {
            City department = await db.Cities.FindAsync(id);
            if (department == null)
            {
                return NotFound();
            }

            return Ok(department);
        }

        // PUT: api/Departments/5
        [ResponseType(typeof(void))]
        [Route("api/Cities")]
        [HttpPut]
        [Authorize(Roles = "DPSValidator")]
        public async Task<IHttpActionResult> PutCity(int id, City city)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != city.ID)
            {
                return BadRequest();
            }

            db.Entry(city).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Departments
        [ResponseType(typeof(Department))]
        [Route("api/Cities")]
        [HttpPost]
        public async Task<IHttpActionResult> PostCity(City city)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Cities.Add(city);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = city.ID }, city);
        }

        // DELETE: api/Departments/5
        [ResponseType(typeof(Department))]
        [Route("api/Departments")]
        public async Task<IHttpActionResult> DeleteDepartment(int id)
        {
            Department department = await db.Departments.FindAsync(id);
            if (department == null)
            {
                return NotFound();
            }

            db.Departments.Remove(department);
            await db.SaveChangesAsync();

            return Ok(department);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DepartmentExists(int id)
        {
            return db.Departments.Count(e => e.ID == id) > 0;
        }

        private bool CityExists(int id)
        {
            return db.Cities.Count(e => e.ID == id) > 0;
        }
    }
}