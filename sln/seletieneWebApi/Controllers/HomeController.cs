﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace seletieneWebApi.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "API Se le tiene";
            //return View();//Use this when you have a page for DPS usage or for presenting Se le tiene Application
            return RedirectToAction("Index", "Help");
        }
        public ActionResult RecoverPassword(string token)
        {
            ViewBag.TokenRecover = token;
            return View();
        }
        public ActionResult PasswordRecoveryResult(string result)
        {
            if(result.Equals("success"))
                ViewBag.Msg = "Contraseña cambiada exitosamente.";
            else
                ViewBag.Msg = "El enlace de recuperación ha vencido. Intenta nuevamente.";
            return View();
        }
    }
}
