﻿using seletieneWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System.Security.Claims;

using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.OAuth;
using CalendarExport.Godaddy;
using System.ServiceModel.Channels;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Specialized;
using System.Text;
using System.Data;
using System.Web;


namespace seletieneWebApi.Controllers
{
    public class DPSController : ApiController
    {
        

        public DPSController()
        {
            using (var context = new SeletieneWebApiContext())
            {
                var roleStore = new RoleStore<IdentityRole>(context);
                var roleManager = new RoleManager<IdentityRole>(roleStore);
                if (roleManager.FindByName(RoleValues.Customer.ToString()) == null)
                    roleManager.Create(new IdentityRole { Name = RoleValues.Customer.ToString() });
                if (roleManager.FindByName(RoleValues.Provider.ToString()) == null)
                    roleManager.Create(new IdentityRole { Name = RoleValues.Provider.ToString() });
                if (roleManager.FindByName(RoleValues.DPSValidator.ToString()) == null)
                    roleManager.Create(new IdentityRole { Name = RoleValues.DPSValidator.ToString() });
                
            }
        }
        public ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        private SeletieneWebApiContext db = new SeletieneWebApiContext();

        /// <summary>
        /// Valida el usuario como proveedor de productos y servicios registrado en el DPS
        /// </summary>
        /// <param name="model">parámetros de validación opcionales</param>
        /// <returns></returns>
        [Route("api/DPS/ValidateProvider")]
        [HttpPost]
        [Authorize(Roles="DPSValidator")]
        public IHttpActionResult ValidateProvider(ValidateProviderBindingModel model)
        {
            var us = db.Users.Find(model.UserDBId);
            if (us == null)
                return NotFound();
            us.DPSValidated = model.UserValidated;
            if (model.EmailConfirmed != null)
                us.EmailConfirmed = model.EmailConfirmed;
            if (model.PhoneNumberConfirmed != null)
                us.PhoneNumberConfirmed = model.PhoneNumberConfirmed;
            db.SaveChanges();
            return Ok();
        }

        public bool validateDPSEndpoint(String cedula, User usuario)
        {
            try
            {
                DpsServiceWeb.WS_MINTIC_SELOTENGO client1 = new DpsServiceWeb.WS_MINTIC_SELOTENGO();
                DataSet respo = client1.TraerInformacionFamilia(cedula);

                if (respo.Tables.Count > 0)
                {
                    if (usuario != null)
                    {

                        var tab = respo.Tables[0];
                        foreach (DataRow row in tab.Rows)
                        {

                            try { usuario.MobileNumber = row["CELULAR"].ToString(); }
                            catch (Exception e1) { }
                            try
                            {
                                if (!String.IsNullOrEmpty(row["CORREOELECTRONICO"].ToString()))
                                    usuario.Email = row["CORREOELECTRONICO"].ToString();
                            }
                            catch (Exception e1) { }
                            try
                            {
                                usuario.PhoneNumber = row["TELEFONO"].ToString();
                                usuario.PhoneNumberConfirmed = true;
                            }
                            catch (Exception e1) { }
                            try { usuario.Name = row["PRIMERNOMBRE"].ToString() + " " + (!String.IsNullOrEmpty(row["SEGUNDONOMBRE"].ToString()) ? (row["SEGUNDONOMBRE"].ToString() + " ") : "") + row["PRIMERAPELLIDO"].ToString(); }
                            catch (Exception e1) { }

                            try
                            {
                                City cus = db.Cities.Where(p => p.PostalCodeGeo.Equals(row["CODMUNICIPIO"].ToString())).FirstOrDefault();
                                if (cus != null)
                                {
                                    usuario.City = cus;
                                    usuario.CityId = cus.ID;
                                    usuario.DepartmentId = cus.DepartmentID;
                                }
                                else
                                {
                                    Department dus = db.Departments.Where(p => p.PostalCodeGeo.Equals(row["CODESTADO"].ToString())).FirstOrDefault();
                                    if (dus != null)
                                    {
                                        usuario.Department = dus;
                                        usuario.DepartmentId = dus.ID;
                                    }
                                }
                            }
                            catch (Exception e) { }

                        }
                    }
                     try
                    {
                        UserManager.AddToRole(usuario.Id, RoleValues.Provider.ToString());
                    }
                    catch (Exception) { }
                     db.SaveChanges();
                    return true;
                }
            }
            catch (Exception eeee)
            {
                return false;
            }

            return false;
        }

        [Route("api/DPS/smsUser")]
        [HttpPost]
        public async Task<IHttpActionResult> newSMSUser(SMSModel mod)
        {
            bool dpsValid = false;

            String doc = "";
            try
            {
                doc = mod.MENSAJE.Split(' ')[1];
            }
            catch { }

            var us = db.Users.Where(p => p.UserId == doc).FirstOrDefault();
            

            if (us == null)
            {
                try
                {

                    using (var client1 = new WebClient())
                    {
                        string fakeemail = doc + "@dpsfake.com";
                        var vals = new NameValueCollection();
                        vals["Email"] = fakeemail;
                        vals["Password"] = doc;
                        vals["Name"] = doc;
                        vals["UserId"] = doc;
                        vals["isSMSUser"] = true.ToString();
                        vals["MobileNumber"] = mod.NUMERO_CELULAR;
                        string currentUr = HttpContext.Current.Request.Url.AbsoluteUri;
                        string currentUP = HttpContext.Current.Request.Url.AbsolutePath;
                        var response = client1.UploadValues(currentUr.Substring(0,currentUr.Count()-currentUP.Count())+
                            "/api/Account", vals);
                        var responseString = Encoding.Default.GetString(response);
                        if(responseString.Contains(AccountController.DPSINVALID))
                            throw new Exception("Not registered");
                    }
                    
                }
                catch (Exception e) { }
            }
            else
            {
                validateDPSEndpoint(us.UserId, us);
            }
            CelmediaService.WSEnvioSMSPortTypeClient client = new CelmediaService.WSEnvioSMSPortTypeClient();

            CustomBinding binding = new CustomBinding(
new CustomTextMessageBindingElement("iso-8859-1", "text/xml", MessageVersion.Soap11),
new HttpsTransportBindingElement());

            client.Endpoint.Binding = binding;

            CelmediaService.WSDataIN toSent = new CelmediaService.WSDataIN()
            {
                user = CELMEDIA_USER,
                password = CELMEDIA_PWD,
                datetime = DateTime.Now.ToString("yyyyMMddHHmmss"),
                movil = mod.NUMERO_CELULAR,
                operador = mod.OPERADOR
            };
            toSent.messageid = toSent.movil + toSent.datetime;

            CelmediaService.WSData wsdata;
            string servmsg = "";

            us = db.Users.Where(p => p.UserId == doc).FirstOrDefault();
            if(us!=null&&us.DPSValidated)
            {
                us.MobileNumber = mod.NUMERO_CELULAR;
                us.PhoneNumberConfirmed = true;
                db.SaveChanges();
                toSent.mensaje = "Registro exitoso en Se le tiene, un operador se comunicara con usted.";
                servmsg += "Usuario dps encontrado";
                try
                {
                    wsdata = client.EnviaSMS(toSent);
                }
                catch (Exception e)
                {
                    servmsg += "\n\n" + e.Message;
                }


                return Ok(servmsg);
            }
            toSent.mensaje = "Usuario no encontrado en los programas de DPS. Por favor comuniquese con la entidad.";

            servmsg = "Usuario dps no encontrado";
            try
            {
                wsdata = client.EnviaSMS(toSent);
            }
            catch (Exception e)
            {
                servmsg += "<br/>" + e.Message + "<br/><br/>Inner:<br/>" + e.InnerException.ToString();
            }
            return Ok(servmsg);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="mod"> Userid en mod corresponde a la cédula</param>
        /// <returns></returns>
        //[Route("api/DPS/smsUser2")]
        //[HttpPost]
        public async Task<IHttpActionResult> newSMSUser2(SMSModel mod)
        {
            bool dpsValid = false;
            JObject blob;

            String doc = "";
            try
            {
                doc = mod.MENSAJE.Split(' ')[1];
            }
            catch { }

            var us = db.Users.Where(p => p.UserId == doc).FirstOrDefault();
            CelmediaService.WSEnvioSMSPortTypeClient client = new CelmediaService.WSEnvioSMSPortTypeClient();

            CustomBinding binding = new CustomBinding(
new CustomTextMessageBindingElement("iso-8859-1", "text/xml", MessageVersion.Soap11),
new HttpsTransportBindingElement());

            client.Endpoint.Binding = binding;
            
            CelmediaService.WSDataIN toSent= new CelmediaService.WSDataIN() { 
                user=CELMEDIA_USER,password=CELMEDIA_PWD,datetime=DateTime.Now.ToString("yyyyMMddHHmmss"),
                movil=mod.NUMERO_CELULAR,operador=mod.OPERADOR
            };
            toSent.messageid = toSent.movil + toSent.datetime;

            //dpsValid = AccountController.validateDPSEndpoint2(doc,null);
            CelmediaService.WSData wsdata;
            string servmsg="";

            //if(dpsValid)
            if(true)
            {
                if (us == null)
                {
                    string fakeemail = "@dpsfake.com";
                    us = await RegisterUserSMSAsync(doc+fakeemail,null,null,null,doc,null,null );
                    us.DPSValidated = true;

                    //AccountController.validateDPSEndpoint2(doc, us);
                    us.MobileNumber = mod.NUMERO_CELULAR;

                    try
                    {

                        using (var client1 = new WebClient())
                        {
                            var vals = new NameValueCollection();
                            vals["Email"] = us.Email;
                            vals["Password"] = doc;
                            vals["Name"] = us.Name;
                            vals["City"] = us.City!=null?us.City.Name:"";
                            vals["Department"] = us.Department!=null?us.Department.Name:"";
                            vals["MobileNumber"] = us.MobileNumber;
                            vals["PhoneNumber"] = us.PhoneNumber;
                            vals["UserId"] = us.UserId;

                            var response = client1.UploadValues("/api/Account", vals);
                            var responseString = Encoding.Default.GetString(response);
                        }
                        throw new Exception("Done");
                        IdentityResult result = await UserManager.CreateAsync(us, doc);

                        if (!result.Succeeded)
                        {
                            if (UserManager.FindByEmail(us.Email) != null)
                                return Conflict();
                            return InternalServerError();
                        }
                        UserManager.AddToRole(us.Id, RoleValues.Provider.ToString());
                    }
                    catch(Exception ex)
                    {
                        if (ex.Message.Equals("Done"))
                            servmsg += "\n Usuario creado exitosamente \n";
                    }

                }
                else
                {
                    us.DPSValidated = dpsValid;
                    //return Ok("Usuario dps encontrado");
              
                }
                toSent.mensaje = "Registro exitoso en Se le tiene, un operador se comunicara con usted.";
                servmsg+= "Usuario dps encontrado";
                try
                {
                    wsdata = client.EnviaSMS(toSent);
                }
                catch(Exception e)
                {
                    servmsg += "\n\n"+e.Message;
                }
                
                
                return Ok(servmsg);
            }
            toSent.mensaje = "Usuario no encontrado en los programas de DPS. Por favor comuniquese con la entidad.";
            
            servmsg = "Usuario dps no encontrado";
            try
            {
                wsdata = client.EnviaSMS(toSent);
            }
            catch (Exception e)
            {
                servmsg += "\n\n" + e.Message;
            }


            return Ok(servmsg);
        }

        private async Task<User> RegisterUserSMSAsync(string email, string firstName, string phone, string cellphone, string cedula, City cit, Department dep)
        {
            var randomPassword = System.Web.Security.Membership.GeneratePassword(10, 5);
            Models.User toRegister = new User
            {
                Name = firstName,
                Email = email,
                UserName = email,
                PhoneNumber = phone,
                MobileNumber = cellphone,
                UserId = cedula,
                RegistrationTime = DateTime.Now,
                City = cit,
                Department = dep,
                ProductsAndServices = new HashSet<ProductService>(),
                Favorites = new HashSet<ProductService>()
            };
            
            IdentityResult result = await UserManager.CreateAsync(toRegister, randomPassword);

            if (result.Succeeded)
            {
                try { 
                UserManager.AddToRole(toRegister.Id, RoleValues.Customer.ToString());
                    }
                catch(Exception ex)
                {
                    Console.Write(ex.Message);
                }
            }
            return toRegister;

        }

        /// <summary>
        /// Valida el producto o servicio como uno válido verificado por el DPS
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("api/DPS/ValidateProductService")]
        [HttpPost]
        [Authorize(Roles = "DPSValidator")]
        public async Task<IHttpActionResult> ValidateProductService(ValidateProductServiceBindingModel model)
        {
            var prod = await db.ProductServices.FindAsync(model.ProductServiceId);
            if (prod == null)
                return NotFound();
            
            prod.DPSValidated = model.validated;
            db.SaveChanges();
            return Ok();
        }


        /// <summary>
        /// Requiere autenticación con rol "DPSValidator". Listado de productos no validados por el DPS
        /// </summary>
        /// <returns>Listado de productos y servicios ofrecidos en la aplicación que no han sido validados por el DPS. La validación es independiente del usuario.</returns>
        [Route("api/DPS/UnvalidatedProductServices")]
        [HttpGet]
        [Authorize(Roles = "DPSValidator")]
        public IQueryable<ProductService> GetUnvalidatedProductServices()
        {
            return db.ProductServices.Where(p=>!p.DPSValidated);
        }


        /// <summary>
        /// Requiere autenticación con rol "DPSValidator". Listado de usuarios no validados por el DPS.
        /// Devuelve suarios registrados no existentes en DPS
        /// </summary>
        /// <returns>Listado de usuarios registrados en la aplicación que no han sido validados como proveedores de productos y servicios válidos en el DPS</returns>
        [Route("api/DPS/UnvalidatedProviders")]
        [HttpGet]
        [Authorize(Roles = "DPSValidator")]
        public IQueryable<User> GetUnvalidatedProviders()
        {
            return db.Users.Where(p => !p.DPSValidated);
        }

        /// <summary>
        /// /// Requiere autenticación con rol "DPSValidator". Listado de usuarios no validados por el DPS
        /// Devuelve usuarios no validados por el callcenter
        /// </summary>
        /// <returns>Listado de usuarios existentes en DPS que no han sido verificados por el callcenter</returns>
        [Route("api/DPS/UnvalidatedCallcenterProviders")]
        [HttpGet]
        [Authorize(Roles = "DPSValidator")]
        public IQueryable<User> GetUnvalidatedCallcenterProviders()
        {
            return db.Users.Where(p => p.DPSValidated && !p.DPSCallcenterValidated);
        }

        //TODO Should be in web.config
        public static string CELMEDIA_USER ="apPsltdps";
        public static string CELMEDIA_PWD ="slti4sp$4pp1t";
    }
}
