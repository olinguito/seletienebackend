﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using seletieneWebApi.Models;
using seletieneWebApi.Providers;
using seletieneWebApi.Results;
using System.Net;
using System.Linq;
using Newtonsoft.Json.Linq;
using System.Data;
using System.Net.Mail;

namespace seletieneWebApi.Controllers
{

    public enum RoleValues { Customer, Provider, DPSValidator }

    [Authorize]
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private const string LocalLoginProvider = "Local";
        public static ApplicationUserManager _userManager;
        private SeletieneWebApiContext db = new SeletieneWebApiContext();

        public AccountController()
        {
            using (var context = new SeletieneWebApiContext())
            {
                var roleStore = new RoleStore<IdentityRole>(context);
                var roleManager = new RoleManager<IdentityRole>(roleStore);
                if (roleManager.FindByName(RoleValues.Customer.ToString()) == null)
                    roleManager.Create(new IdentityRole { Name = RoleValues.Customer.ToString() });
                if (roleManager.FindByName(RoleValues.Provider.ToString()) == null)
                    roleManager.Create(new IdentityRole { Name = RoleValues.Provider.ToString() });
                if (roleManager.FindByName(RoleValues.DPSValidator.ToString()) == null)
                    roleManager.Create(new IdentityRole { Name = RoleValues.DPSValidator.ToString() });
                context.SaveChanges();
            }
        }

        public AccountController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }


        // GET api/Account/UserInfo
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("ExternalLoginInfo")]
        public UserInfoViewModel GetUserInfo()
        {
            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            return new UserInfoViewModel
            {
                Email = User.Identity.GetUserName(),
                HasRegistered = externalLogin == null,
                LoginProvider = externalLogin != null ? externalLogin.LoginProvider : null
            };
        }
        // GET api/Account/ManageInfo?returnUrl=%2F&generateState=true
        [Route("ManageInfo")]
        public async Task<ManageInfoViewModel> GetManageInfo(string returnUrl, bool generateState = false)
        {
            IdentityUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            
            if (user == null)
            {
                return null;
            }

            List<UserLoginInfoViewModel> logins = new List<UserLoginInfoViewModel>();

            foreach (IdentityUserLogin linkedAccount in user.Logins)
            {
                logins.Add(new UserLoginInfoViewModel
                {
                    LoginProvider = linkedAccount.LoginProvider,
                    ProviderKey = linkedAccount.ProviderKey
                });
            }

            if (user.PasswordHash != null)
            {
                logins.Add(new UserLoginInfoViewModel
                {
                    LoginProvider = LocalLoginProvider,
                    ProviderKey = user.UserName,
                });
            }

            return new ManageInfoViewModel
            {
                LocalLoginProvider = LocalLoginProvider,
                Email = user.UserName,
                Logins = logins,
                ExternalLoginProviders = GetExternalLogins(returnUrl, generateState)
            };
        }

        //PUT api/Account/ChangeRol
        /// <summary>
        /// Available rols: ["customer","provider","dpsvalidator"]... "customer" is default
        /// </summary>
        /// <param name="userDbId"></param>
        /// <param name="newRole"></param>
        /// <returns></returns>
        [Route("ChangeRol")]
        [Authorize]//TODO rol restringido, no el mismo que cambia
        public IHttpActionResult ChangeRol(string userDbId, RoleValues newRole)
        {
            var userChanged = db.Users.Find(userDbId);
            if(userChanged!=null)
            {
                if(userChanged.satisfyDataForProvider())
                {
                    UserManager.AddToRole(userDbId, newRole.ToString());
                }
                else
                {
                    return BadRequest("User does not satisfy requirements of provider");
                }
            }
            return Ok("User role changed to "+userChanged.Roles.FirstOrDefault().ToString());
        }

        // POST api/Account/ChangePassword
        [Route("ChangePassword")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword,
                model.NewPassword);
            
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/RecoverPassword
        [Route("RecoverPasswordEmail")]
        [AllowAnonymous]
        [HttpPost]
        public async Task<IHttpActionResult> RecoverPasswordEmail(string email)
        {
            var us=db.Users.Where(p => p.UserName.Equals((string)email)).FirstOrDefault();
            if(us!=null)
            {
                string ansa = await UserManager.GeneratePasswordResetTokenAsync(us.Id);
                long ctime = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
                ansa+=";"+email+";"+ctime.ToString();
                string tosend=HttpServerUtility.UrlTokenEncode(System.Text.Encoding.ASCII.GetBytes (ansa));
                
                if (ansa.Split(';').Count() > 3)
                    return BadRequest("Lo sentimos, ha sucedido un error, por favor vuelva a solicitar la recuperación de contraseña");
                string encodedemail = ((string)email).Split('@')[0];

                if (encodedemail.Count() > 1)
                {
                    string astss = "";
                    for (int i = 1; i < encodedemail.Count(); i++)
                    {
                        astss += "*";

                    }
                    encodedemail = ((string)email).Replace(encodedemail.Substring(1), astss);
                }
                else encodedemail = (string)email;
                string basepat = getbasepath();
                string linkrecover=basepat+"/Home/RecoverPassword?token="+tosend;
                string mailMessage = "Hola, " + us.Name + ":<br/><br/>Para recuperar tu contraseña en Se le tiene sigue <a href=\"" + linkrecover + "\">este enlace</a>. Debe acceder en menos de 24 horas.<br/><br/>Departamento para la prosperidad social. <br/>Prosperidad para todos";
                sendRecoverEmail(mailMessage, email, "Recuperación de contraseña en Se le tiene");

                return Ok("Se ha enviado un correo a "+encodedemail+" con el enlace de recuperación.");
            }
            else
                return BadRequest("Usuario no encontrado");
        }

        private string getbasepath()
        {
            string currentUr = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Path);
            string currentUP = HttpContext.Current.Request.Url.AbsolutePath;
            return currentUr.Substring(0, currentUr.Count() - currentUP.Count());
        }

        private async void sendRecoverEmail(string mailMessage,string email,string subject)
        {
            using (SmtpClient client = new SmtpClient())
            using (MailMessage mail = new MailMessage())
            {
                mail.To.Add(email);
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                mail.Subject = subject;
                mail.IsBodyHtml = true;
                mail.Body = mailMessage;
                client.Send(mail);   
            }
            
        }
        [Route("RecoverPassword")]
        [AllowAnonymous]
        [HttpPost]
        public IHttpActionResult RecoverPassword(RecoverPasswordModel mod)
        {
            string token = mod.Token;
            string newpassword = mod.NewPassword;
            string toreceive = System.Text.Encoding.Default.GetString(HttpServerUtility.UrlTokenDecode(token));
            string[] paramets = toreceive.Split(';');
            if (paramets.Count() !=3)
                return BadRequest("Lo sentimos, ha sucedido un error, por favor vuelva a solicitar la recuperación de contraseña");
            string tok = paramets[0];
            string email = paramets[1];
            long spantime = long.Parse(paramets[2]);
            long diff = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond - spantime;
            long tfhours=1000*60*60*24;
            if(diff>tfhours)
            {
                return Redirect("/Home/PasswordRecoveryResult?result=fail");
            }

            var us = db.Users.Where(p => p.UserName.Equals(email)).FirstOrDefault();

            if (us != null)
            {
                IdentityResult re = UserManager.ResetPassword(us.Id, tok, newpassword);
                if (re.Succeeded)
                {
                    //HttpContext.Current.Response.RedirectLocation = "/Home/PasswordRecoveryResult?result=success";
                    return Redirect(getbasepath()+"/Home/PasswordRecoveryResult?result=success");
                }
                else return Redirect(getbasepath() + "/Home/PasswordRecoveryResult?result=fail");
            }
            else
            {
                //HttpContext.Current.Response.RedirectLocation = "/Home/PasswordRecoveryResult?result=fail";
                return BadRequest("Usuario no encontrado");
            }
        }

        // POST api/Account/SetPassword
        [Route("SetPassword")]
        public async Task<IHttpActionResult> SetPassword(SetPasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/AddExternalLogin
        [Route("AddExternalLogin")]
        public async Task<IHttpActionResult> AddExternalLogin(AddExternalLoginBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            AuthenticationTicket ticket = AccessTokenFormat.Unprotect(model.ExternalAccessToken);

            if (ticket == null || ticket.Identity == null || (ticket.Properties != null
                && ticket.Properties.ExpiresUtc.HasValue
                && ticket.Properties.ExpiresUtc.Value < DateTimeOffset.UtcNow))
            {
                return BadRequest("External login failure.");
            }

            ExternalLoginData externalData = ExternalLoginData.FromIdentity(ticket.Identity);

            if (externalData == null)
            {
                return BadRequest("The external login is already associated with an account.");
            }

            IdentityResult result = await UserManager.AddLoginAsync(User.Identity.GetUserId(),
                new UserLoginInfo(externalData.LoginProvider, externalData.ProviderKey));

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/RemoveLogin
        [Route("RemoveLogin")]
        public async Task<IHttpActionResult> RemoveLogin(RemoveLoginBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result;

            if (model.LoginProvider == LocalLoginProvider)
            {
                result = await UserManager.RemovePasswordAsync(User.Identity.GetUserId());
            }
            else
            {
                result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(),
                    new UserLoginInfo(model.LoginProvider, model.ProviderKey));
            }

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // GET api/Account/ExternalLogin
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [AllowAnonymous]
        [Route("ExternalLogin", Name = "ExternalLogin")]
        public async Task<IHttpActionResult> GetExternalLogin(string provider, string error = null)
        {
            if (error != null)
            {
                return Redirect(Url.Content("~/") + "#error=" + Uri.EscapeDataString(error));
            }

            if (!User.Identity.IsAuthenticated)
            {
                return new ChallengeResult(provider, this);
            }

            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            if (externalLogin == null)
            {
                return InternalServerError();
            }

            if (externalLogin.LoginProvider != provider)
            {
                return new ChallengeResult(provider, this);
            }

            User user = await UserManager.FindAsync(new UserLoginInfo(externalLogin.LoginProvider,
                externalLogin.ProviderKey));

            bool hasRegistered = user != null;

            if (hasRegistered)
            {
                
                 ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(UserManager,
                    OAuthDefaults.AuthenticationType);

                AuthenticationProperties properties = ApplicationOAuthProvider.CreateProperties(user.UserName);
                Authentication.SignIn(properties, oAuthIdentity);
            }
            else
            {
                IEnumerable<Claim> claims = externalLogin.GetClaims();
                ClaimsIdentity identity = new ClaimsIdentity(claims, OAuthDefaults.AuthenticationType);
                Authentication.SignIn(identity);                
            }

            return Ok();
        }

        // GET api/Account/ExternalLogins?returnUrl=%2F&generateState=true
        [AllowAnonymous]
        [Route("ExternalLogins")]
        public IEnumerable<ExternalLoginViewModel> GetExternalLogins(string returnUrl, bool generateState = false)
        {
            IEnumerable<AuthenticationDescription> descriptions = Authentication.GetExternalAuthenticationTypes();
            List<ExternalLoginViewModel> logins = new List<ExternalLoginViewModel>();

            string state;

            if (generateState)
            {
                const int strengthInBits = 256;
                state = RandomOAuthStateGenerator.Generate(strengthInBits);
            }
            else
            {
                state = null;
            }

            foreach (AuthenticationDescription description in descriptions)
            {
                ExternalLoginViewModel login = new ExternalLoginViewModel
                {
                    Name = description.Caption,
                    Url = Url.Route("ExternalLogin", new
                    {
                        provider = description.AuthenticationType,
                        response_type = "token",
                        client_id = Startup.PublicClientId,
                        redirect_uri = new Uri(Request.RequestUri, returnUrl).AbsoluteUri,
                        state = state
                    }),
                    State = state
                };
                logins.Add(login);
            }

            return logins;
        }


        //GET api/Account/me
        [Route("")]
        [HttpGet]
        [Authorize]
        public UserSimplified GetUser()
        {
            User user = UserManager.FindById(User.Identity.GetUserId());
            return user.GetSimplified();
        }

        //GET api/Account/me
        [Route("Favorites")]
        [HttpGet]
        public ICollection<ProductService> GeMyFavorites()
        {
            User user = UserManager.FindById(User.Identity.GetUserId());
            return user.Favorites;
        }
        //GET api/Account/me
        [Route("ProductServices")]
        [HttpGet]
        public ICollection<ProductService> GeMyProductsAndServices()
        {
            User user = UserManager.FindById(User.Identity.GetUserId());
            return user.ProductsAndServices;
        }

        //GET /api/account
        [Route("")]
        [HttpPut]
        public IHttpActionResult UpdateUser(User updated)
        {
            if (updated == null) { return BadRequest("Nothing to change"); }
            User ut= UserManager.FindById(User.Identity.GetUserId());
                
            if (ut == null)
            {return BadRequest("User not found");}

            User u = db.Users.Find(ut.Id);
            if (!String.IsNullOrEmpty(updated.UserId))
                u.UserId = updated.UserId;
            if (!String.IsNullOrEmpty(updated.Name))
                u.Name = updated.Name;
            if (!String.IsNullOrEmpty(updated.PhoneNumber))
                u.PhoneNumber = updated.PhoneNumber;
            if (!String.IsNullOrEmpty(updated.MobileNumber))
                u.MobileNumber = updated.MobileNumber;
            if (updated.DPSValidated)
                u.DPSValidated = updated.DPSValidated;//never invalidated
            
            db.SaveChanges();
            return Ok(updated);
        }

        /// <summary>
        /// Registra un nuevo usuario
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("")]
        [HttpPost]
        public async Task<IHttpActionResult> Register(RegisterBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                foreach (ModelState modelState in ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        HttpResponseMessage response = Request.CreateErrorResponse(HttpStatusCode.ExpectationFailed, error.ErrorMessage);
                        throw new HttpResponseException(response);
                    }
                }
                
            }
            City cit = null;
            Department dep = null;
            
            
            
            if(model.CityID!=0)
            {
                cit = db.Cities.Find(model.CityID);
                if(cit!=null) dep = db.Departments.Find(cit.DepartmentID);
            }
            
            if (cit==null&&model.City != null)
            {
                var cits = db.Cities.Where(i=>i.Name.Equals(model.City.ToUpper()));
                if (cits.Count()>0)
                {
                    cit = cits.FirstOrDefault();
                    dep = db.Departments.Find(cit.DepartmentID);
                }
            }

            if (model.DepartmentID != 0)
            {
                dep = db.Departments.Find(model.DepartmentID);
            }
            if (dep == null && model.Department != null)
            {
                var deps = db.Departments.Where(i => i.Name.Equals(model.Department.ToUpper()));

                if (deps.Count() == 0)
                {
                    dep = new Department() { Name = model.Department.ToUpper(), Cities = new HashSet<City>() };
                    db.Departments.Add(dep);
                    db.SaveChanges();
                }
                else
                {
                    dep = deps.FirstOrDefault();
                }
            }
            
            var user = new User()
            {
                UserName = model.Email,
                Email = model.Email,
                UserId = model.UserId,
                Name = model.Name,
                PhoneNumber = model.PhoneNumber,
                MobileNumber = model.MobileNumber,
                RegistrationTime = DateTime.Now,
                ProductsAndServices = new HashSet<ProductService>(),
                Favorites = new HashSet<ProductService>()
            };
            user.DPSValidated = validateDPSEndpoint(user.UserId, null);
            if(model.isSMSuser&&!user.DPSValidated)
            {
                return Ok(DPSINVALID);
            }
            if (cit != null)
            {
                //user.City = cit;
                user.CityId=cit.ID;
                if (dep != null)
                {
                    //user.Department = dep;
                    user.DepartmentId = dep.ID;
                }
            }

            IdentityResult result = await UserManager.CreateAsync(user, model.Password);
            
            if (!result.Succeeded)
            {
                if (UserManager.FindByEmail(model.Email) != null)
                    return Conflict(); 
                return GetErrorResult(result);
            }
            UserManager.AddToRole(user.Id, RoleValues.Customer.ToString());

            

            return Ok();
        }

        /// <summary>
        /// A usar por un Validador de DPS en caso de validar un usuario en el programa manualmente
        /// </summary>
        /// <param name="cedula">Cédula del usuario</param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        public bool validateDPSEndpoint(String cedula, User usuario)
        {
            try
            {
                DpsServiceWeb.WS_MINTIC_SELOTENGO client1 = new DpsServiceWeb.WS_MINTIC_SELOTENGO();
                DataSet respo = client1.TraerInformacionFamilia(cedula);

                if (respo.Tables.Count > 0)
                {
                    if (usuario != null)
                    {

                        var tab = respo.Tables[0];
                        foreach (DataRow row in tab.Rows)
                        {

                            try { usuario.MobileNumber = row["CELULAR"].ToString(); }
                            catch (Exception e1) { }
                            try {
                                if(!String.IsNullOrEmpty(row["CORREOELECTRONICO"].ToString()))
                                    usuario.Email = row["CORREOELECTRONICO"].ToString(); }
                            catch (Exception ) { }
                            try
                            {
                                usuario.PhoneNumber = row["TELEFONO"].ToString();
                                usuario.PhoneNumberConfirmed = true;
                            }
                            catch (Exception e1) { }
                            try { usuario.Name = row["PRIMERNOMBRE"].ToString() + " " +(!String.IsNullOrEmpty(row["SEGUNDONOMBRE"].ToString())?(row["SEGUNDONOMBRE"].ToString()+" "):"" )+ row["PRIMERAPELLIDO"].ToString(); }
                            catch (Exception e1) { }

                            try
                            {
                                City cus = db.Cities.Where(p => p.PostalCodeGeo.Equals(row["CODMUNICIPIO"].ToString())).FirstOrDefault();
                                if (cus != null)
                                {
                                    usuario.City = cus;
                                    usuario.CityId = cus.ID;
                                    usuario.DepartmentId = cus.DepartmentID;
                                }
                                else
                                {
                                    Department dus = db.Departments.Where(p => p.PostalCodeGeo.Equals(row["CODESTADO"].ToString())).FirstOrDefault();
                                    if (dus != null)
                                    {
                                        usuario.Department = dus;
                                        usuario.DepartmentId = dus.ID;
                                    }
                                }
                            }
                            catch (Exception e) { }

                        }
                    }
                    try
                    {
                        UserManager.AddToRole(usuario.Id, RoleValues.Provider.ToString());
                    }
                    catch (Exception) { }
                    db.SaveChanges();
                    return true;
                }
            }
            catch(Exception eeee)
            {   
                return false;
            }
            
            return false;
        }


        private async Task<FacebookUserViewModel> VerifyFacebookAccessToken(string accessToken)
        {
            FacebookUserViewModel fbUser = null;
            var path = "https://graph.facebook.com/me?access_token=" + accessToken;
            var client = new HttpClient();
            var uri = new Uri(path);
            var response = await client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                fbUser = Newtonsoft.Json.JsonConvert.DeserializeObject<FacebookUserViewModel>(content);
            }
            return fbUser;
        }

        // POST /FacebookLogin
        /// <summary>
        /// Registra un nuevo usuario con credenciales de facebook. Si el correo existe el usuario podrá registrarse con contraseña o facebook.
        /// </summary>
        /// <param name="mod">recibe el token otorgado por facebook y el email a registrar en Se le tiene</param>
        /// <returns>Bearer token</returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("FacebookLogin")]
        public async Task<IHttpActionResult> FacebookLogin(RegisterExternalBindingModel mod)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("facebook token and email required");
            }

            var tokenExpirationTimeSpan = TimeSpan.FromDays(14);
            Models.User user = null;
            // Get the fb access token and make a graph call to the /me endpoint
            var fbUser = await VerifyFacebookAccessToken(mod.token);
            if (fbUser == null)
            {
                return BadRequest("Invalid OAuth access token");
            }
            // Check if the user is already registered
            string em = fbUser.Email ?? fbUser.Username ?? mod.Email;
            string nombre = fbUser.Name ?? mod.Name;
            user = await UserManager.FindByNameAsync(em);
            // If not, register it
            if(user==null)
            {
                user = db.Users.Where(p => p.FacebookUserId.Equals(fbUser.ID)).FirstOrDefault() ;
            }
            else
            {
                user.FacebookUserId = fbUser.ID;
                db.SaveChanges();
            }
            if (user == null)
            {
                var randomPassword = System.Web.Security.Membership.GeneratePassword(10, 5);
                City cc = db.Cities.Find(mod.cityId);
                Department dd = db.Departments.Find(cc==null?mod.departmentId:cc.DepartmentID);
                user = await RegisterUserFbAsync(em, randomPassword, fbUser.ID, nombre,mod.PhoneNumber,mod.MobileNumber,mod.UserId,cc,dd);
            }
            else
            {
                user.FacebookUserId = fbUser.ID;
                db.SaveChanges();
            }

            // Sign-in the user using the OWIN flow
            var identity = new ClaimsIdentity(Startup.OAuthBearerOptions.AuthenticationType);
            identity.AddClaim(new Claim(ClaimTypes.Name, user.UserName, null, "Facebook"));
            // This is very important as it will be used to populate the current user id 
            // that is retrieved with the User.Identity.GetUserId() method inside an API Controller
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Id, null, "LOCAL_AUTHORITY"));
            AuthenticationTicket ticket = new AuthenticationTicket(identity, new AuthenticationProperties());
            var currentUtc = new Microsoft.Owin.Infrastructure.SystemClock().UtcNow;
            ticket.Properties.IssuedUtc = currentUtc;
            ticket.Properties.ExpiresUtc = currentUtc.Add(tokenExpirationTimeSpan);
            var accesstoken = Startup.OAuthBearerOptions.AccessTokenFormat.Protect(ticket);
            Request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", accesstoken);
            Authentication.SignIn(identity);

            // Create the response building a JSON object that mimics exactly the one issued by the default /Token endpoint
            JObject blob = new JObject(
                new JProperty("userName", user.UserName),
                new JProperty("access_token", accesstoken),
                new JProperty("token_type", "bearer"),
                new JProperty("expires_in", tokenExpirationTimeSpan.TotalSeconds.ToString()),
                new JProperty(".issued", ticket.Properties.IssuedUtc.ToString()),
                new JProperty(".expires", ticket.Properties.ExpiresUtc.ToString())
            );
            // Return OK
            // TODO post to dps endpoint to check status!
            return Ok(blob);
        }

        private async Task<User> RegisterUserFbAsync(string email, string randomPassword, string facebookUserId,string firstName,string phone,string cellphone,string cedula, City cit,Department dep)
        {
            Models.User toRegister = new User
            {
                Name = firstName,
                Email = email,
                UserName=email,
                PhoneNumber=phone,
                MobileNumber=cellphone,
                UserId=cedula,
                FacebookUserId = facebookUserId,
                RegistrationTime = DateTime.Now,
                City=cit,
                Department=dep,
                ProductsAndServices = new HashSet<ProductService>(),
                Favorites = new HashSet<ProductService>()
            };
            IdentityResult result = await UserManager.CreateAsync(toRegister,randomPassword);

            if (result.Succeeded)
            {
                UserManager.AddToRole(toRegister.Id, RoleValues.Customer.ToString());
            }
            return toRegister;
            
        }

        // POST api/Account/RegisterExternal
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("RegisterExternal")]
        public async Task<IHttpActionResult> RegisterExternal(RegisterExternalBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var info = await Authentication.GetExternalLoginInfoAsync();
            if (info == null)
            {
                return InternalServerError();
            }

            var user = new User() { UserName = model.Email, Email = model.Email };

            IdentityResult result = await UserManager.CreateAsync(user);
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            result = await UserManager.AddLoginAsync(user.Id, info.Login);
            if (!result.Succeeded)
            {
                return GetErrorResult(result); 
            }
            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                UserManager.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Helpers

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        private class ExternalLoginData
        {
            public string LoginProvider { get; set; }
            public string ProviderKey { get; set; }
            public string UserName { get; set; }

            public IList<Claim> GetClaims()
            {
                IList<Claim> claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.NameIdentifier, ProviderKey, null, LoginProvider));

                if (UserName != null)
                {
                    claims.Add(new Claim(ClaimTypes.Name, UserName, null, LoginProvider));
                }

                return claims;
            }

            public static ExternalLoginData FromIdentity(ClaimsIdentity identity)
            {
                if (identity == null)
                {
                    return null;
                }

                Claim providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

                if (providerKeyClaim == null || String.IsNullOrEmpty(providerKeyClaim.Issuer)
                    || String.IsNullOrEmpty(providerKeyClaim.Value))
                {
                    return null;
                }

                if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
                {
                    return null;
                }

                return new ExternalLoginData
                {
                    LoginProvider = providerKeyClaim.Issuer,
                    ProviderKey = providerKeyClaim.Value,
                    UserName = identity.FindFirstValue(ClaimTypes.Name)
                };
            }
        }

        private static class RandomOAuthStateGenerator
        {
            private static RandomNumberGenerator _random = new RNGCryptoServiceProvider();

            public static string Generate(int strengthInBits)
            {
                const int bitsPerByte = 8;

                if (strengthInBits % bitsPerByte != 0)
                {
                    throw new ArgumentException("strengthInBits must be evenly divisible by 8.", "strengthInBits");
                }

                int strengthInBytes = strengthInBits / bitsPerByte;

                byte[] data = new byte[strengthInBytes];
                _random.GetBytes(data);
                return HttpServerUtility.UrlTokenEncode(data);
            }
        }

        #endregion

        public static string DPSINVALID = "User not valid in dps";
    }
}
